<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\ArticlesController;
use App\Http\Controllers\PagesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PagesController::class, 'home']);

/*
Route::get('/', function(){

});

/*
Route::get('/', function(){
    return view('welcome');
});

Route::get('/about', function(){
    $articles = App\Models\Article::take(3)->latest()->get();

    return view('about', [
        'articles' => $articles
    ]);
});

Route::get('articles', [ArticlesController::class, 'index'])->name('articles.index');
Route::post('articles', [ArticlesController::class, 'store']);
Route::get('articles/create', [ArticlesController::class, 'create']);
Route::get('articles/{article}', [ArticlesController::class, 'show'])->name('articles.show');
Route::get('articles/{article}/edit', [ArticlesController::class, 'edit']);
Route::put('articles/{article}', [ArticlesController::class, 'update']);
*/



// GET /articles
// GET /articles/:id
// POST /articles
// PUT /articles/:id
// DELETE /articles/:id/

// GET /videos
// GET /videos/create
// POST /videos
// GET /videos/2
// GET /videos/2/edit
// PUT /videos/2
// DELETE /videos/2

Route::get('/contact', function(){
    return view('contact');
});

Route::get('posts/{post}', [PostsController::class, 'show']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
