<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Example;
use App\Models\ExampleFacade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind('example', function(){
            return new Example();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
