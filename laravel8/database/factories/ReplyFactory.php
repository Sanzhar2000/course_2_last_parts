<?php

use App\Models\Reply;
use App\Models\User;
use App\Models\Conversation;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factories $factory */

$factory->define(Reply::class, function (Faker $faker) {
    $user = User::orderByRaw('RAND()')->first();
    $conversation = Conversation::orderByRaw('RAND()')->first();

    return [
        'user_id' => $user->id,
        'conversation_id' => $conversation->id,
        'body' => $faker->sentence,
    ];
});
