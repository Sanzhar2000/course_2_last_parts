<?php


use App\Models\Conversation;
use App\Models\User;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factories $factory */

$factory->define(Conversation::class, function (Faker $faker) {

    $user = User::orderByRaw('RAND()')->first();

    return [
        'user_id' => $user->id,
        'title' => $faker->sentence,
        'body' => $faker->text,
    ];
});
