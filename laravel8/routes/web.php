<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\PaymentsController;
use App\Http\Controllers\ConversationController;
use App\Http\Controllers\ConversationBestReplyCController;
use App\Http\Controllers\UserNotificationsController;

auth()->loginUsingId(1);

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/reports', function () {
    return 'the secret reports';
})->middleware('can:view_reports');

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('conversations', [ConversationController::class, 'index']);

Route::get('conversations/{conversation}', [ConversationController::class, 'show'])->middleware('can:view,conversation');

Route::post('best-replies/{reply}', [ConversationBestReplyController::class, 'store']);

Auth::routes();

Route::get('/contact', [ContactController::class, 'show']);

Route::post('/contact', [ContactController::class, 'store']);

Route::get('/payments/create', [PaymentsController::class, 'create'])->middleware('auth');

Route::post('payments', [PaymentsController::class, 'store'])->middleware('auth');

Route::get('notifications', [UserNotificationsController::class, 'show'])->middleware('auth');



//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('auth');
