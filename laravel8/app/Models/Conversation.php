<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Reply;

class Conversation extends Model
{
    //
    use HasFactory;

    public function user() {
        return $this->hasMany(Reply::class);
    }
}
