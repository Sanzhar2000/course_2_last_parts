<?php


namespace App\Http\Controllers;
use App\Models\Conversation;


class ConversationController extends Controller
{
    public function index()
    {
        return view('conversations.index', [
            'conversations' => Conversation::all()
        ]);
    }

    public function show(Conversation $conversation)
    {
        //dd($conversation);

        return view('conversations.show', [
            'conversation' => $conversation
        ]);
    }
}

